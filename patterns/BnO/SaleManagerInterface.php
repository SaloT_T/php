<?php

interface SaleManagerInterface
{
    public function addCustomer(Customer $c);
    public function removeCustomer( Customer $c);
    public function notify();
}