<?php

//require 'Computer.php';

abstract class ComputerBuilder
{
    protected $computer;
    
    public function getComputer()
    {
        return $this->computer;
    }
    public function createNewComputer()
    {
        $this->computer = new Computer();
    }
    abstract public function buildCPU($cpu);
    abstract public function buildGPU($gpu);
    abstract public function buildRAM($ram);
    abstract public function buildHDD($hdd);
    abstract public function buildMBoard($mboard);
    abstract public function buildPower($power);
    abstract public function buildSunit($sunit);
    
}