<?php

//require 'ComputerBuilder.php';

class CustomComputer extends ComputerBuilder
{
    public function buildCPU($cpu)
    {
        $this->computer->setCPU($cpu);
    }
    public function buildGPU($gpu)
    {
        $this->computer->setGPU($gpu);
    }
    public function buildRAM($ram)
    {
        $this->computer->setRAM($ram);
    }
    public function buildHDD($hdd)
    {
        $this->computer->setHDD($hdd);
    }
    public function buildMBoard($mboard)
    {
        $this->computer->setMBoard($mboard);
    }
    public function buildPower($power)
    {
        $this->computer->setPower($power);
    }
    public function buildSunit($sunit)
    {
        $this->computer->setSunit($sunit);
    }
    
    
    
}