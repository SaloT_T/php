<?php

class Computer
{
    private $cpu = '';
    private $gpu = '';
    private $ram = '';
    private $hdd = '';
    private $mboard = '';
    private $power = '';
    private $sunit = '';
    
    public function setCPU($cpu)
    {
        $this->cpu = $cpu;
    }
    public function setGPU($gpu)
    {
        $this->gpu = $gpu;
    }
    public function setRAM($ram)
    {
        $this->ram = $ram;
    }
    public function setHDD($hdd)
    {
        $this->hdd = $hdd;
    }
    public function setMBoard($mboard)
    {
        $this->mboard = $mboard;
    }
    public function setPower($power)
    {
        $this->power = $power;
    }
    public function setSunit($sunit)
    {
        $this->sunit = $sunit;
    }
    
    public function getCPU()
    {
       return $this->cpu;
    }
    public function getGPU()
    {
       return $this->gpu;
    }
    public function getRAM()
    {
       return $this->ram;
    }
    public function getHDD()
    {
       return  $this->hdd;
    }
    public function getMBoard()
    {
       return $this->mboard;
    }
    public function getPower()
    {
       return $this->power;
    }
    public function getSunit()
    {
       return $this->sunit;
    }
}
