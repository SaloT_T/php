<?php

//require 'ComputerBuilder.php';

class Assemble
{
    private $computerBuilder;
    
    public function setComputerBuilder(ComputerBuilder $cb)
    {
        $this->computerBuilder = $cb;
    }
    public function runAssemble($data)
    {
        $this->computerBuilder->createNewComputer();
        $this->computerBuilder->buildCPU($data['cpu']);
        $this->computerBuilder->buildGPU($data['gpu']);
        $this->computerBuilder->buildRAM($data['ram']);
        $this->computerBuilder->buildHDD($data['hdd']);
        $this->computerBuilder->buildMBoard($data['mboard']);
        $this->computerBuilder->buildPower($data['power']);
        $this->computerBuilder->buildSunit($data['sunit']);
    }
    
    public function getAssemble()
    {
        return $this->computerBuilder->getComputer();
    }
}