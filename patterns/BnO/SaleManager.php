<?php

class SaleManager implements SaleManagerInterface
{
    private $customers = [];
    public function addCustomer(Customer $c)
    {
        $this->customers[] = $c;
    }
    public function removeCustomer(Customer $c){}
    public function notify()
    {
        
        foreach($this->customers as $customer){
            $customer->eventHandler();
        }
    }
}