<?php

spl_autoload_register(function ($class){
    
    include "classes/$class.php";
    
});

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
    <form method="get">
        <input type="text" name="type">
        <button >Send</button>
    </form>
    <?php
    $type = $_GET['type'];
//    echo $type;
    $carF = new CarFactory();
    $obj = $carF->getCar($type);
    ?>
    <h2><?php echo $obj->name($type);  ?></h2>
    <p><?php  echo $obj->desc();  ?></p>
    
</body>
</html>