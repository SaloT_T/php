<?php
    interface Car
{
    public function go();
    public function name($name);
    public function desc();
}