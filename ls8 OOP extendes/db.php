<?php

class Db
{
    private static $instance = null;
    private $connection;
    public static function getInstance()
    {
        if(is_null(self::$instance)){
            self::$instance = new self();
        }
        return self::$instance;
    }
    private function __construct(){
        $this->connection = new mysqli('localhost','root','','jsstore');
    }
    private function __clone(){}
    
    public function query($sql)
    {
        return $this->connection->query($sql);
    }
    
    private function __destruct()
    {
        return $this->connection;
    }
}

$db = Db::getInstance();
