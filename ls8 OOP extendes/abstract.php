<?php

abstract class Car
{
    abstract public function turn($side);
    
    public function go()
    {
        return "go";
    }
}


class Audi extends Car 
{
    public function turn($side)
    {
        return $side;
    }
}

$audi = new Audi();
echo $audi->turn('left');
echo "<br>";
echo $audi->go();