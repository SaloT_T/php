<?php
    
abstract class Continent
{
    protected $name;
    protected $location;
    protected $numerosity;
    abstract public function setlocation($latitude, $langitude);
    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }
     public function setNumerosity($count)
    {
        $this->numerosity = $count; 
    }
    
    public function getNumerosity()
    {
        return $this->numerosity;
    }
}


abstract class Country extends Continent
{
    protected $continent;
  
    public function setContinent($name)
    {
        $this->name = $name;
    }
    
    public function getContinent()
    {
        return $this->name;
    }

    public function setlocation($latitude, $langitude)
    {
        $location = "latitude: " .$latitude ." ;". "langitude: " .$langitude ." ;";
        $this->location = $location;    
    }
    
    public function getlocation()
    {
        return $this->location;
    }
}

abstract class Region extends Country
{
    protected $country;

      public function setCountry($name)
    {
        $this->name = $name;
    }
    
    public function getCountry()
    {
        return $this->name;
    }
}
class Eurasia extends Continent
{
     public function setlocation($latitude, $langitude)
    {
        $location = "latitude: " .$latitude ." ;". "langitude: " .$langitude ." ;";
        $this->location = $location;    
    }
}
class Ukraine extends Country
{

}

class DneproObl extends Region
{
}

$Eurasia = new Eurasia();
$Eurasia->setName('Eurasia');
echo $Eurasia->getName() . '<br>';
$Eurasia->setNumerosity('4,618*10^9');
echo $Eurasia->getNumerosity(). '<br>';
$Ukraine = new Ukraine();
$Ukraine->setName('Ukraine');
echo $Ukraine->getName(). '<br>';
$Ukraine->setNumerosity('45*10^6');
echo $Ukraine->getNumerosity(). '<br>';
$DneproObl = new DneproObl();
$DneproObl->setName('Dnepropetrovskaya Oblast\'');
echo $DneproObl->getName(). '<br>';
$DneproObl->setNumerosity('3,286 *10^6');
echo $DneproObl->getNumerosity(). '<br>';


