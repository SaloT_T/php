<?php

interface CarInterface
{

    public function go();
    public function turn();
    public function engine();
}

interface SportCarInterface
{

    public function turbo();
    

}
interface FlyCarInterface
{

    public function fly();
    

}

class Tt implements CarInterface
{

    public function go()
    {
       return $this->go(); 
    }
    public function turn()
    {
        return $this->turn();
    }
    public function engine()
    {
        return $this->engine();
    }
}


class TimeMachine
{
    private $car;
    public function __construct(FlyCarInterface $car)
    {
        $this->car = $car;
    }
    public function goToFuture()
    {
       return $this->car->fly();
    }

}

class Delorian implements CarInterface, FlyCarInterface
{
    public function go()
    {
       return $this->go(); 
    }
    public function turn()
    {
        return $this->turn();
    }
    public function engine()
    {
        return $this->engine();
    }
    public function fly()
    {
        return "fly";
    }
}

class Slavuta implements FlyCarInterface, CarInterface
{

    public function go()
    {
        
    }
     public function turn()
    {
        return $this->turn();
    }
    public function engine()
    {
        return $this->engine();
    }
    public function fly()
    {
        return "Letat'";
    }
}

$tm = new TimeMachine(new Slavuta());
echo $tm->goToFuture();