<?php


interface Image
{
    public function crop();

}

class Jpg implements Image
{
    public function crop()
    {
        return "crop jpg file";
    }
}

class Png implements Image
{
    public function crop()
    {
        return "crop png file";
    }

}

class Gif implements Image
{
    public function crop()
    {
        return "crop gif file";
    }

}

class ImageFactory
{
    public function getImage($type)
    {
        switch($type){

            case 'jpg': return new Jpg();
                break;
            case 'png': return new Png();
                break;
            case 'gif': return new Gif;
                break;
        }
    }
}

$type = 'jpg';
$imageF = new ImageFactory();
$obj = $imageF->getImage($type);
echo $obj->crop();

