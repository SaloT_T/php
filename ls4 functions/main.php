<?php
    $weather = [
        'type' => 'cloudy',
        'code' => 32,
        'temp' => 35,
        'humidity' => '10%',
        'pressure' => 25,
        'location' => 
            [
                'country' => 'Ukraine',
                'region' => 'Dnipro',
                'city' => 'Dnipro'
            ]
    ];
    
    $str = json_encode($weather);
    header('Content-type: application/json');    
    echo $str;
    
     ?>   