<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <?php
        echo "<pre>";
        $m = 1;
        $y = 2017;
        for($i = 1; $i < 12; $i++)
        {
            echo "<table>";
            echo "<tr>";
            echo "<th>Пн</th>";
            echo "<th>Вт</th>";   
            echo "<th>Ср</th>";  
            echo "<th>Чт</th>"; 
            echo "<th>Пт</th>";
            echo "<th>Сб</th>";
            echo "<th>Вс</th>";
            echo "</tr>";   
            echo "<tr>";
            for($j = 1; $j < 31; $j++)
            {
                $offset = date('N', strtotime("{$y}-{$i}-01"));
                $d = date('j', mktime(0,0,0,$m,$j,$y));
                
                if($j%7 == 0){echo "<tr>";};
                echo "<td>" . $d . "</td>";
                if($j%7 == 0){echo "</tr>"; echo "<tr>";};
                $m++;
            };
        echo "</table>";
    };
    echo "</pre>";
    ?>
</body>
</html>