<!DOCTYPE html>
<html lang="en">

<head>
   <link href="style/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php 
    
 #   function showCalendar($month){
        // Получим информацию о днях. 
        // Нам нужен текущий, первый и последний день месяца. 
        $today    = getdate();
        $firstDay = getdate(mktime(0,0,0,$today['mon'],1,$today['year']));
        $lastDay  = getdate(mktime(0,0,0,$today['mon']+1,0,$today['year']));
        // Создадим таблицу с заголовком
       echo '<table>';
       echo '  <tr><th colspan="7">'.$today['month']." - ".$today['year'].
   		 "</th></tr>";
        echo '<tr class="days">';
        echo '  <td>Mo</td><td>Tu</td><td>We</td><td>Th</td>';
        echo '  <td>Fr</td><td>Sa</td><td>Su</td></tr>';
        // Покажем первую строку календаря
        echo '<tr>';
        for($i=1;$i<$firstDay['wday'];$i++){
            echo '<td>&nbsp;</td>';
        }
        $actday = 0;
        for($i=$firstDay['wday'];$i<=7;$i++){
            $actday++;
            if ($actday == $today['mday']) {
                $class = ' class="actday"';
            } else {
                $class = '';
            }
            echo "<td$class>$actday</td>";
        }
        echo '</tr>';
    //    // Получим количество полных недель текущего месяца
        $fullWeeks = floor(($lastDay['mday']-$actday)/7);
    //
        for ($i=0;$i<$fullWeeks;$i++){
            for ($j=0;$j<7;$j++){
                $actday++;
                if ($actday == $today['mday']) {
                    $class = ' class="actday"';
                } else {
                    $class = '';
                }
                echo "<td$class>$actday</td>";
            }
            echo '</tr>';
        }
        //Покажем последние дни месяца
        if ($actday < $lastDay['mday']){
            echo '<tr>';
            for ($i=0; $i<7;$i++){
                $actday++;
                if ($actday == $today['mday']) {
                    $class = ' class="actday"';
                } else {
                    $class = '';
                if ($actday <= $lastDay['mday']){
                    echo "<td$class>$actday</td>";
                }
                else {
                    echo '<td>&nbsp;</td>';
                }
            }
            
            echo '</tr>';
        }
        
        echo '</table>';
    }
    
?>

</body>
</html> 