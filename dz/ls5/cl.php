<?php $y = date('Y'); 
        
 for($i = 1; $i <= 12; $i++){ 
        
    $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $i, $y); 
    $offset = date('N', strtotime("{$y}-{$i}-01")); 
    echo "{$y}-{$i}-01";
    
    $monthName = date('F', strtotime("{$y}-{$i}-01")); 
    $daysName = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']; 
    $monthName = 
            [
                'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
            ];
    echo "<table>";
    echo "<tr>";
    for($w = 0; $w < 7; $w++){
        echo "<th>".$daysName[$w]."</th>";
      }
    echo "</tr>";
    echo "<tr>";
    --$offset; 
    for($o = 0; $o < $offset; $o++){
        echo "<td></td>";
    }
    for($d = 1; $d <= $daysInMonth; $d++){
        echo "<td>". $d."</td>";
        if(($d + $offset) % 7 == 0){
            echo "</tr><tr>";
        }
    }
    for($d = 0; ($d + $offset + $daysInMonth) % 7 != 0; $d++){
            echo "<td></td>";
    }
    echo "</tr>";
    echo "</table>";
    };